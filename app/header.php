<!DOCTYPE html>

<?php
    require_once './classes/App.php';
    $app = new App();
    $param = $app->isHome();
    $page = (count($param) > 1) ? $param[count($param)-1] : 'home';
?>

<html class="no-js" lang="en-US" xmlns:og="http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Iquini</title>

	<meta name="description" content="Iquini - Cor do Ano 2022">
	<meta name="author" content="Iquini">

    <meta property="og:type" content="website">
    <meta property="og:title" content="Iquini">
    <meta property="og:url" content="<?php echo 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>">
    <meta property="og:description" content="Iquini - Cor do Ano 2022">
    <meta property="og:image" content="<?php echo $app->getBaseUrl(); ?>img/content/share.jpg">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="./img/content/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="<?php echo $app->getBaseUrl(); ?>dist/main.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;900&display=swap" rel="stylesheet">
</head>
<body>
