<div class="bg-layout">
    <div class="flex-bg index-1">
        <img class="img-left" src="./img/layout/bg-left.svg" alt="">
        <img class="img-right" src="./img/layout/bg-right.svg" alt="">
    </div>
    <?php require_once('partials/blocos/topo-lp.php');?>
    <?php require_once('partials/blocos/cor-do-ano-2022.php');?>
    <?php require_once('partials/blocos/aconchego.php');?>
    <?php require_once('partials/blocos/joana-lira.php');?>
    <?php require_once('partials/blocos/cores-complementares.php');?>
    <?php require_once('partials/blocos/maos-no-stencil.php');?>
    <?php require_once('partials/blocos/galeria-imagens.php');?>
    <?php require_once('partials/blocos/footer.php');?>
</div>