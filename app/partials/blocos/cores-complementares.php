<div class="block-text cores-complementares">
    <div class="content-center">
        <div class="title-block">
            <h2>
                Cores <br>
                <strong>
                Complementares
                </strong>
            </h2>
        </div>
        <div class="bloco-cores">
            <div class="item-cores bloco1-1">
                <span class="cor-1"><p class="c-black">Mossoró 1652 M</p></span>
                <span class="cor-2"><p>Boi-bumbá 1079 C</p></span>
                <span class="cor-3"><p>Pedra da Estrela 2261 C</p></span>
            </div>
            <div class="item-cores bloco1-2">
                <span class="cor-1"><p>Boi-bumbá 1079 C</p></span>
                <span class="cor-2"><p>Mangabeiras 1250 P</p></span>
                <span class="cor-3"><p class="c-black">Paraguaçu 1950 P</p></span>
            </div>
            <div class="item-cores bloco1-3">
                <span class="cor-1"><p>Jaqueira 1927 C</p></span>
                <span class="cor-2"><p>Pico do Nebana 2045 C</p></span>
                <span class="cor-3"><p>Boi-bumbá 1079 C</p></span>
            </div>
            <div class="item-cores bloco1-4">
                <span class="cor-1"><p>Maçaranduba 1115 C</p></span>
                <span class="cor-2"><p>Boi-bumbá 1079 C</p></span>
                <span class="cor-3"><p>Canela 1715 M</p></span>
            </div>
        </div>

        <div class="bloco-cores">
            <div class="item-cores bloco2-1">
                <span class="cor-1"><p>Santa Rosa 1162 M</p></span>
                <span class="cor-2"><p>Colatina 1793 C</p></span>
                <span class="cor-3"><p>Boi-bumbá 1079 C</p></span>
            </div>
            <div class="item-cores bloco2-2">
                <span class="cor-1"><p>Boi-bumbá 1079 C652 M</p></span>
                <span class="cor-2"><p class="c-black">Banana 1598 C</p></span>
                <span class="cor-3"><p>Salvador 1742 M</p></span>
            </div>
            <div class="item-cores bloco2-3">
                <span class="cor-1"><p class="c-black">Pecém 1538 P</p></span>
                <span class="cor-2"><p>Boi-bumbá 1079 C</p></span>
                <span class="cor-3"><p>Chimarrita 1183 M</p></span>
            </div>
            <div class="item-cores bloco2-4">
                <span class="cor-1"><p>Mato Verde 1416 C</p></span>
                <span class="cor-2"><p class="c-black">Jericoacoara 1414 M</p></span>
                <span class="cor-3"><p>Boi-bumbá 1079 C</p></span>
            </div>
        </div>

        <div class="bloco-cores">
            <div class="item-cores bloco3-1">
                <span class="cor-1"><p class="c-black">Limão 1568 M</p></span>
                <span class="cor-2"><p>Boi-bumbá 1079 C</p></span>
                <span class="cor-3"><p>Areia Vermelha 1052 C</p></span>
            </div>
            <div class="item-cores bloco3-2">
                <span class="cor-1"><p>Boi-bumbá 1079 C652 M</p></span>
                <span class="cor-2"><p>Maracatu 1205 C</p></span>
                <span class="cor-3"><p>Olivina 2226 C</p></span>
            </div>
            <div class="item-cores bloco3-3">
                <span class="cor-1"><p>Caranguejo 1794 C</p></span>
                <span class="cor-2"><p>Frevo 1164 C</p></span>
                <span class="cor-3"><p>Boi-bumbá 1079 C</p></span>
            </div>
            <div class="item-cores bloco3-4">
                <span class="cor-1"><p>Maragogi 1449 M</p></span>
                <span class="cor-2"><p>Boi-bumbá 1079 C</p></span>
                <span class="cor-3"><p class="c-black">Bem-te-vi 1582 C</p></span>
            </div>
        </div>
      
    </div>
</div>
