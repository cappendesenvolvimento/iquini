<div class="block-text index-2">
    <div class="content-center">
        <div class="title-block">
            <h2>
            Conheça <br>
            <strong>
                Joana Lira
            </strong>
            </h2>
        </div>
        <figure class="img-joana-mobile only-mobile">
            <img src="./img/content/joana-1.jpg" alt="">
        </figure>
        <div class="flex-block-text">
            <div class="text-block">
                <p>
                Nas palavras de Joana, “Quando preciso me definir rapidamente, digo que trabalho com artes visuais e design de afeto. Design que afeta. Se tenho mais uma brechinha de fala, completo e enfatizo: meu trabalho só ganha força e sentido quando pode ser ferramenta de transformação e melhoria para a vida do outro. Mas acredito em demasia que toda definição é limitante, enquanto o ser humano é mutante - e pode ser sempre mais, não menos.”
                    <br><br>
                Formada em design gráfico em 1997, trabalhou de 2001 a 2011 na criação da cenografia do Carnaval do Recife, experiência formadora que deu escala e consistência ao seu trabalho e está sintetizada no livro Outros Carnavais (2008).
                <br><br>
                A partir de 1999, já radicada em São Paulo, pesquisa suportes diversos, da cerâmica ao meio digital, em projetos artísticos alimentados por referências que vêm tanto da história da arte e da cultura pernambucana, quanto de seus interesses cotidianos.
                <br><br>
                Seus trabalhos foram vistos em exposições individuais, como Quando a vida é uma euforia (Instituto Tomie Ohtake, São Paulo, 2018, e Museu Cais do Serão, Recife, 2019). Quando tudo explode (Sesc Santo André, 2017) e Bichos Aloprados (Recife, 1997). Também estiveram em coletivas referenciais como Design Brasileiro Hoje: Fronteiras (Museu de Arte Moderna, São Paulo, 2010) e Design para Todos (5a Bienal Brasileira de Design, Florianópolis, 2015), além de integrarem mostras sobre design brasileiro e ibero-americano na Alemanha, China, Bélgica e Espanha.
                </p>
            </div>
            <div class="img-block only-imgs">
                <div class="flex-images">
                    <figure class="only-desk">
                        <img src="./img/content/joana-1.jpg" alt="">
                    </figure>
                    <figure>
                        <img src="./img/content/joana-2.jpg" alt="">
                    </figure>
                </div>
            </div>
        </div>
        
    </div>
</div>
