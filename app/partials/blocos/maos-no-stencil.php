<div class="block-text index-2">
    <div class="content-center">
        <div class="title-block">
            <h2>
            Agora, <br>
            <strong>
                mãos <br>
                no stencil
            </strong>
            </h2>
        </div>
        <div class="flex-block-text">
            <div class="text-block">
                <p>
                    Para obter o melhor resultado, o stencil deve ser impresso em acetato, já que esse material confere a resistência necessária. <br>
                    <strong>
                        Com o stencil em mãos, você poderá escolher a cor e aplicar onde preferir, seguindo esses passos:
                    </strong>
                </p>
                <br><br>
                <p>
                    <ul>
                        <li>
                            A primeira coisa que você precisa fazer é decidir a cor que será usada no stencil.
                        </li>
                        <li>
                            Feito isso, comece a pintura. Fixe o stencil na parede ou superfície que preferir com fita adesiva para evitar que ele se desloque.
                        </li>
                        <li>
                            Carregue o rolo de pintura com a tinta, mas não exagere. O excesso de tinta pode escorrer e borrar o desenho.
                        </li>
                        <li>
                            É importante não colocar muita pressão na hora de aplicar a tinta. Passe o rolo suavemente sobre o stencil.
                        </li>
                        <li>
                            Para uma pintura completa de parede, comece pelo canto superior esquerdo e finalize no canto inferior direito.
                        </li>
                        <li>
                            E, se por acaso, a tinta passar do stencil e sujar a parede, limpe imediatamente.
                        </li>
                        <li>
                            Após concluir a pintura, retire o stencil com cuidado.
                        </li>
                    </ul>
                </p>
            </div>
            <div class="img-block card">
                <a href="#" class="card-flex">
                    <figure>
                        <span class="hover"></span>
                        <img src="./img/layout/stencil.svg" alt="">
                    </figure>
                    <p>
                        Faça o download do Stencil
                    </p>
                </a>
            </div>
        </div>
        
    </div>
</div>
