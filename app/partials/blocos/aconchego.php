<div class="block-text only-text index-2">
    <div class="content-center">
        <div class="title-block">
            <h2>
            Um enredo <br>
            em <strong>aconchego.</strong>
            </h2>
        </div>
        <div class="flex-block-text">
            <div class="text-block flex">
                <p>
                Toda vez que desenho um pásaro, sinto um vento ligeiro e potente que causa uma sensação de liberdade, de novas descobertas, de ganho de amplitude e de abertura para o novo. <br><br>

                A arte criada, com exclusividade para celebrar e difundir o laranja-rosa como a Cor do Ano da Iquine em 2022, tem um ser voador como elemento central em composição com um sol-estrela, o amor de um coração palpitante e outras folhas e flores que enlaçam todo esse enredo em aconchego.
                </p>
                <p>
                Essa arte deseja levar alegria, calor, entusiasmo e vibração para dentro e fora da casa das pessoas. Deseja também estimular a retomada de grandes sonhos com novos planos de voo, permeados pelo cuidado, pelo receber e pela possibilidade de festejar o reencontro adiado.<br><br>

                Esta criação foi desenvolvida para ser reproduzida em módulo vazado, na técnica do stencial, com a versatilidade de ser aplicada como desenho único ou como padronagem - de diversas maneiras - em ambientes internos ou externos.
                <br><br><br>

                <span>Joana <strong>Lira</strong>.</span>

                </p>
            </div>
        </div>
    </div>
</div>
