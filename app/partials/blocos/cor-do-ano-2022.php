<div class="block-text only-text index-2">
    <div class="content-center">
        <div class="title-block">
            <h2>
            Já conhece <br>
            a <strong>
            Cor do Ano <br>
            2022?
            </strong>
            </h2>
        </div>
        <div class="flex-block-text">
            <div class="text-block flex">
                <p>
                Moderna, ousada e versátil, a cor do ano eleita pela Iquine instiga a autoconfiança em um cenário marcado de retomadas de sonhos e planos.<br><br>
    
                A Tintas Iquine, maior empresa de tintas do país com capital 10% nacional, apresenta sua cor do ano 2002: Boi-Bumbá, um vibrante laranja-rosa que transmite alegria e instigaa confiança, sensações que os brasileiros buscarão ainda mais para o próximo ano, que promete ser a retomada de grandes sonhos e planos, como viagens, rencontros e grandes descobertas, após um longo período de incertezas.<br><br>
    
                A Iquine aposta na experiência que as cores podem transmitir através de sensações, por isso, a cor de 2002 busca despertar o poder interior. A motivação, a intensidade e a atitude para transformaçõ espesoais e sociais são algumas das definições e emoções que a tonalidade pode emanar. Segundo o estudo da psicologia das cores, a cor laranja transmite calor e entusiasmo, mensagens revigorantes ao ambiente.<br><br>
    
                O tom rosa escuro, por sua vez, proporciona um toque estimulante, resultado da mistura de cores quentes e frias. Logo, a energia proporcionada pelo laranja-rosa traduz esse otimismo e confiança pela retomada em 2022. Em um período que promete muitos rencontros, o tom vibrante da cor eleita pela Iquine, fortalece a afetividade e renovação de sonhos que o novo momento pede.<br><br>
                </p>
                <p>
                A cor eleita pela Iquine respira brasilidade e se alinha às tendências de decoração urbanista e do maximalismo, que valoriza o mix de cores e estampas. Por conta do tom vibrante e das sensações positivas que a cor proporciona, ela é versátil e pode ser usada em salas, quartos, banheiros, cozinhas, escritórios e corredores, seja nas paredes, janelas, móveis ou portas, com pinturas completas ou setorizadas por meio de formas geométricas, arcos ou até desenhos abstratos.<br><br>
    
                Para chegar à definição da cor do ano, a Iquine encomendou estudos aprofundados do bureau de tendências da Calzada Fox, agência britânica de pesquisas especializada na influência das cores. Há quase dois anos, o mundo passou a olhar mais para dentro de suas casas, buscando formas de dar mais cor e vida para aqueles ambientes que redobraram sua importânciaem tempos de confinamento.<br><br>
    
                Agora, com a iminente chegada de 2022, abraçamos a posibilidade de, baseado no contexto e tendências, olharmos com carinho e cuidado para fora de casa. Juntos poderemos voltar a viver a cidade e os rencontros, acompanhados de uma cor energizante.
                </p>
            </div>
        </div>
    </div>
</div>
