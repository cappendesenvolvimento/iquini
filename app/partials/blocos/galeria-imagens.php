<div class="galeria-imagens">
    <div class="content-center">
        <div class="flex">
            <figure><img src="./img/content/galeria-1.jpg" alt=""></figure>
            <figure><img src="./img/content/galeria-2.jpg" alt=""></figure>
            <figure><img src="./img/content/galeria-3.jpg" alt=""></figure>
            <figure><img src="./img/content/galeria-4.jpg" alt=""></figure>
            <figure><img src="./img/content/galeria-5.jpg" alt=""></figure>
        </div>
    </div>
</div>