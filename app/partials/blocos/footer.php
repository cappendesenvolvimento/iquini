<footer>
    <div class="content-center">
        <div class="flex">
            <p class="copy">
            Copyright © 2022. Iquine. Todos os direitos reservados. As cores deste site são apenas referências, podendo existir divergência entre as 
            cores apresentadas na tela e as dos produtos comercializados. Recomendamos que utilize as cartelas de cores disponíveis nas lojas de 
            tintas para visualizar e escolher corretamente a cor desejada.
            </p>
            <a href="https://www.cappen.com/" target="_blank">
                Site by <span>Cappen</span>
            </a>
        </div>
    </div>
</footer>