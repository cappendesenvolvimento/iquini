**Começando um novo projeto**

Para começar com o template zerado, abrir a pasta "htdocs" ou equivalente no terminal e rodar a seguinte linha de comando, lembrando de mudar _NOME_DA_PASTA_ para o nome do projeto.

`git clone --depth=1 --branch=master https://bitbucket.org/cappendesenvolvimento/boilerplate-site.git NOME_DA_PASTA && cd NOME_DA_PASTA && rm -rf .git && git init`

Após iniciar o git na pasta do projeto, fazer o apontamento para a url do repositório:

`git remote add origin https://usuario@bitbucket.org/cappendesenvolvimento/projeto.git`

**Antes de rodar o projeto**

-   Buscar **[##]** e editar o que aparecer pra ser específico do projeto. É recomendado utilizar search and replace:
    -   [##]Cliente: nome do cliente / projeto, estilizado de acordo;
    -   [##]Fonte: fonte principal usada no projeto.
-   Ajustar os valores dos atributos `name` e `description` do arquivo `package.json`.
-   Dentro da pasta do projeto, rodar `bash bin/setup`. Checar se o terminal está como bash.

**Antes de continuar o projeto**

-   Criar a imagem `share.jpg` na pasta `/img/content` de acordo com a identidade visual do projeto, pra ser usada como imagem de compartilhamento nos sites sociais.
-   Adicionar os favicons em `/img/favicons`.
-   Acessar o projeto em `/app`.

**Organização de pastas**

-   CSS: `/app/scss`.
-   JS: `/app/scripts`.
-   Imagens: `/img/layout` para imagens usadas como elementos de layout no CSS e `/img/content` para imagens usadas como conteúdo no HTML, podendo-se criar subpastas pra páginas específicas.

**Finalizando o projeto**

-   "Minificar" todas as imagens do projeto usando [Squoosh](https://squoosh.app) ou similar.
