const webpack = require('webpack')
const WriteFilePlugin = require('write-file-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const globImporter = require('node-sass-glob-importer')
const path = require('path')

module.exports = {
	devServer: {
		port: 8080
	},
	entry: [path.join(__dirname, 'app', 'scripts', 'main.js'), path.join(__dirname, 'app', 'scss', 'main.scss')],
	output: {
		path: path.join(__dirname, 'app', 'dist'),
		filename: 'app.bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				options: {
					presets: ['@babel/preset-env']
				}
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							url: false,
							sourceMap: true
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							ident: 'postcss',
							plugins: [require('autoprefixer')()],
							sourceMap: true
						}
					},
					{
						loader: 'sass-loader',
						options: {
							importer: globImporter(),
							includePaths: ['node_modules'],
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
				exclude: /node_modules/
			}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		}),
		new WriteFilePlugin(),
		new MiniCssExtractPlugin({
			filename: 'main.css'
		})
	]
}
